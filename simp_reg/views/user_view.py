from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.permissions import AllowAny
from ..forms.user_serializer import UserSerializer
from django.contrib.auth.models import User


class UserView(ViewSet):
    @staticmethod
    def list(request: Request) -> Response:
        user = User.objects.all()
        return Response(UserSerializer(user, many=True).data)

    @staticmethod
    def retrieve(request: Request, pk: int) -> Response:
        page = get_object_or_404(User, pk=pk)
        return Response(UserSerializer(page).data)
